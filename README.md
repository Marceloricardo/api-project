# DashBus API
### API de Consumo de Dados do DASHBUS 

O **DashBus** é um sistema que visa fazer a análise e mineração de dados do STP (Sistema de Transporte Público de Teresina), e os exibir em uma _dashboard_ (daí o uso do termo _**dash**_ no nome do sistema).

Essa documentação visa explicar a arquitetura do sistema, informar características do mesmo e explanar o seu uso.

Enjoy it :D

## Como rodar o sistema
	yarn install
	yarn dev
	
## Tecnologias utilizadas
- HTTPS
	> Hyper Text Transfer Protocol Secure
- JSON
	> Java Script Object Notation

## Arquitetura da API

A API é composta por **2 camadas**.
- Aplicação NodeJS:
	> Endpoints;
	> Controladores;
	> Modelos;
	> Etc...
- Banco de dados MongoDB:
	> Armazenamento de dados para a aplicação.
### Modelo arquitetura
![Alt text](https://bitbucket.org/Marceloricardo/api-project/raw/3da4eccdc9b3d6486a78eec474f30fd2058a233f/documents/arq_diagram.PNG "Arquitetura da API")

### Diagrama de classes
![Alt text](https://bitbucket.org/Marceloricardo/api-project/raw/3da4eccdc9b3d6486a78eec474f30fd2058a233f/documents/class_diagram.jpeg "Diagrama de classes")