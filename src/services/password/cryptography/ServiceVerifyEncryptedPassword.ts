import * as bcrypt from 'bcrypt';

class ServiceVerifyEncryptedPassword {
    async verifyencryptPassword(password: string, originalPassword: string): Promise<boolean> {
        const verify = await bcrypt.compare(password, originalPassword)
        return verify;

    }

}
export { ServiceVerifyEncryptedPassword };

