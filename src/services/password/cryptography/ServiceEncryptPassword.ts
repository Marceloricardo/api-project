import * as bcrypt from 'bcrypt';


class ServiceEncryptPassword {
    async encryptPassword(password: string): Promise<string> {
        const newpassword = await bcrypt.hash(password, 8)
        return newpassword;

    }
}
export { ServiceEncryptPassword }

