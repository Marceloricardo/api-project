

class ServiceGeneratePassword {
    async generatePassword(): Promise<string> {
        const password = await Math.random().toString(36).slice(-10);
        return password;
    }
}
export { ServiceGeneratePassword }

