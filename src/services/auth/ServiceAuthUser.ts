import jwt from "jsonwebtoken"

interface IToken {
    id: string | undefined,
    email: string | undefined,
    name: string | undefined
}
class ServiceAuthUser {
    async generateToken(data: IToken): Promise<string> {
        const token = jwt.sign(data, process.env.KEY_TOKEN_USER || '', { expiresIn: 10000 });
        return token;
    }
}

export { ServiceAuthUser };