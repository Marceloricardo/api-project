import { NextFunction, Response,Request } from "express"
import {verify,} from 'jsonwebtoken';
interface IPayload {
    sub: string
    id:string
}
export  function  validToken(request: Request, response: Response, next: NextFunction):Response|void {
    const authHeader = request.headers.authorization;
    if (!authHeader) {
        return response.status(401).json(
            {
                error: 'Unauthorized',

                status: "error"
            }
        )
    }
    const [ , token ] = authHeader.split(' ')
    try{
       const payload = verify(token, process.env.KEY_TOKEN_USER as string)as IPayload
        request.body.user = payload
       return next()
    }
    catch{
        return response.status(500).json({
            status: "error",
            message: "Internal Server Error"
        })}
    }