//import { Ticketing } from "../../../entities/Ticketing";

import {IFindByTripsWeeks, IFindByTripsOperator, IFindByTripsRelationship, IFindByTripsType, IFindByTripsTypeWeek, IFindAverageTripsWeeks } from "./implements/interfaces";

export interface ITicketingRepository {
    findByTripsOperator( dateStart?: string, dateEnd?:string,codLine?: string ):Promise<IFindByTripsOperator>;
    findByTripsType(dateStart?: string, dateEnd?:string, codLine?: string):Promise<IFindByTripsType>;
    findAllTrips(dateStart?: string, dateEnd?:string,codLine?: string ):Promise<number>;
    findByTripsRelationship(x:string,y:string,dateStart?:string, dateEnd?:string,codLine?: string):Promise<IFindByTripsRelationship>;
    findByTripsWeek( dateStart?: string, dateEnd?:string, codLine?: string):Promise<IFindByTripsWeeks>;
    findByTripsTypeWeek(type:string, dateStart?: string, dateEnd?:string, codLine?: string):Promise<IFindByTripsTypeWeek>;
    findAverageTripsWeek(dateStart?: string, dateEnd?: string,codLine?: string,type?:string): Promise<IFindAverageTripsWeeks>
}