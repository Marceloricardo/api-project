import { ITicketingRepository } from "../ITicketingRepository";
import TicketingModel from  '../../../../models/ticketing.model';
import {  IFindByTripsWeeks, IFindByTripsOperator, IFindByTripsRelationship, IFindByTripsType, IFindByTripsTypeWeek, IFindAverageTripsWeeks } from "./interfaces";
import { HelperValidateDate } from "../../../../helper/HelperValidateDate";

export class DBTicketingRepository implements ITicketingRepository {
    async findByTripsTypeWeek(type:string, dateStart?: string, dateEnd?: string, codLine?: string): Promise<IFindByTripsTypeWeek|any> {
        const helperValidateDate = new HelperValidateDate();
            const {datestart,dateend} = await helperValidateDate.validateDate(dateStart||'',dateEnd||'');
            const filtercodLine = codLine? {codLine:{$gt:codLine}}: null;

            const ticketing = await TicketingModel.aggregate([ 
                {$match: {dateCollect: {$gte:datestart, $lt: dateend},
            ...filtercodLine
            }},
                
                { 
                    $group: {
                     "_id": "$dayWeek",
                     count: {$sum: `$${type}`} 
                 },
             } ,
             {
                 $project:{
                    dayWeek:"$_id",
                    count: "$count",
                  _id:false},
                  
                  },
                  { $sort : { count : -1} }
            ]);
            return ticketing;
        }
  
  async  findByTripsRelationship(x: string, y: string, dateStart?: string, dateEnd?: string,codLine?: string): Promise<IFindByTripsRelationship|any> {
    const helperValidateDate = new HelperValidateDate();
    const {datestart,dateend} = await helperValidateDate.validateDate(dateStart||'',dateEnd||'');
    const filtercodLine = codLine? {codLine:{$gt:codLine}}: null;

    const ticketing = await TicketingModel.aggregate([
        {$match: {dateCollect: {$gte:datestart, $lt: dateend},
    ...filtercodLine
    }},
     {
         $project:{
            dateCollect: "$dateCollect",
             x:`$${x}`,
             y: `$${y}`,
          _id:false}
        ,
        
        },
 ]);
 return ticketing;
    }
async  findAllTrips(dateStart?: string, dateEnd?: string, codLine?: string): Promise<number> {
        const helperValidateDate = new HelperValidateDate();
        const filtercodLine = codLine? {codLine:{$gt:codLine}}: null;
        const {datestart,dateend} = await helperValidateDate.validateDate(dateStart||'',dateEnd||'');
        const findAllTrips = await TicketingModel.countDocuments({
            dateCollect: {$gte:datestart, $lt: dateend},
           ...filtercodLine
        }).exec();
        return findAllTrips
    }
async findByTripsWeek(dateStart?: string, dateEnd?: string, codLine?: string): Promise<IFindByTripsWeeks|any> {
        const helperValidateDate = new HelperValidateDate();
            const {datestart,dateend} = await helperValidateDate.validateDate(dateStart||'',dateEnd||'');
            const filtercodLine = codLine? {codLine:{$gt:codLine}}: null;
            const ticketing = await TicketingModel.aggregate([ 
                {$match: {
                    dateCollect: {$gte:datestart, $lt: dateend},
                    ...filtercodLine
            }},
                
                { 
                    $group: {
                     "_id": "$dayWeek",
                     count: { $sum: 1 }
                 },
             } ,
             {
                 $project:{
                    dayWeek:"$_id",
                     count : "$count",
                  _id:false},
                  
                  },
                  { $sort : { count : -1} }
            ]);
            return ticketing;
        }
      
 async findByTripsType(dateStart?: string, dateEnd?: string ,codLine?: string): Promise<IFindByTripsType|any> {
    const helperValidateDate = new HelperValidateDate();
    const {datestart,dateend} = await helperValidateDate.validateDate(dateStart||'',dateEnd||'');
    const filtercodLine = codLine? {codLine:{$gt:codLine}}: null;

    const ticketing = await TicketingModel.aggregate([
        {$match: {dateCollect: {$gte:datestart, $lt: dateend},
    ...filtercodLine
    }},
        { 
            $group: {
             "_id": null,
             passengers: { $sum: "$passengers"},
             entire: { $sum: "$entire"},
             gratuity: { $sum: "$gratuity"},
             student: { $sum: "$student"}
         },
         
     } ,
          { $sort : { count : -1} }
 ])
    return  ticketing
  }

  async  findByTripsOperator(dateStart?: string, dateEnd?: string,codLine?: string): Promise<IFindByTripsOperator|any> {
    const helperValidateDate = new HelperValidateDate();
    const {datestart,dateend} = await helperValidateDate.validateDate(dateStart||'',dateEnd||'');
    const filtercodLine = codLine? {codLine:{$gt:codLine}}: null;
        const ticketing = await TicketingModel.aggregate([
            {$match: {dateCollect: {$gte:datestart, $lt: dateend},
            ...filtercodLine
        },
        },
           { 
               $group: {
                "_id": "$nameOperator",
                count: { $sum: 1 }
            },
        } ,
        {
            $project:{
                nameOperator:"$_id",
                count : "$count",
             _id:false},
            },
             { $sort : { count : -1} }
    ])
       return  ticketing
    }
   
   
    async findAverageTripsWeek(dateStart?: string, dateEnd?: string, type?:string, codLine?: string,): Promise<IFindAverageTripsWeeks|any>{
        const helperValidateDate = new HelperValidateDate();
        const {datestart,dateend} = await helperValidateDate.validateDate(dateStart||'',dateEnd||'');
        const filtercodLine = codLine? {codLine:{$gt:codLine}}: null;
        const averageTripsWeek = await TicketingModel.aggregate([ 
            {$match: {dateCollect: {$gte:datestart, $lt: dateend},
        ...filtercodLine
        }},
            
            { 
                $group: {
                 "_id": "$dayWeek",
                 count: {$avg: `$${type}`} 
             },
         } ,
         {
             $project:{
                dayWeek:"$_id",
                count: "$count",
              _id:false},
              
              },
              { $sort : { count : -1} }
        ]);
        return averageTripsWeek;

    }



}