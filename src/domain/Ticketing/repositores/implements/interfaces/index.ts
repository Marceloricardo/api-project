

export interface IFindByTripsOperator{
    nameOperator:string;
    count: number
}

export interface IFindByTripsType{
    passengers:number,
    entire: number,
    gratuity: number,
    student: number
}

export interface IFindAllTrips{
    count:string;
}

export interface IFindByTripsRelationship{
  x:string;
  y:string;
}

interface IFindByTripsWeek{
    constpredict:number;
    x1predict: number
}

export interface IFindByTripsWeeks extends Array<IFindByTripsWeek>{
    IFindByTripsWeek:IFindByTripsWeek
}
export interface IFindByTripsTypeWeek{
    dayWeek:string;
    count: number
}

 interface IFindAverageTripsWeek{
    dayWeek:string;
    count: number
}


 export interface IFindAverageTripsWeeks extends Array<IFindAverageTripsWeek>{
    IFindAverageTripsWeek:IFindAverageTripsWeek
 }
