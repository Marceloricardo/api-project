import { ITicketingRepository } from "../../repositores/ITicketingRepository";
import { ITripsTicketingAllRequestDTO } from "./TripsTicketingAllDTO";

export class TripsTicketingAllUseCase {
    constructor(
        private ticketingRepository: ITicketingRepository,
    ) {
    }
    async execute(data: ITripsTicketingAllRequestDTO): Promise<number> {
        const findAllTrips = await this.ticketingRepository.findAllTrips(data.dateStart,data.dateEnd,data.codLine);


        return findAllTrips;
        

    }
}