import { Request, Response } from "express";
import { TripsTicketingAllUseCase } from "./TripsTicketingAllUseCase";

export class TripsTicketingAllController {
    constructor(
        private tripsTicketingAllUseCase: TripsTicketingAllUseCase,
    ) { }
    async handle(request: Request, response: Response): Promise<Response> {
 
            const { dateStart, dateEnd, codLine } = request.params;

            const tripsticketing = await this.tripsTicketingAllUseCase.execute({
                dateStart,
                dateEnd,
                codLine
            });
                return response.status(200).json(tripsticketing);
                // #swagger.responses[200] = { description: 'number' }

      
       
    }
}