interface ITripsTicketingAllRequestDTO {
    dateStart:string;
    dateEnd: string;
    codLine?: string
}


export { ITripsTicketingAllRequestDTO };