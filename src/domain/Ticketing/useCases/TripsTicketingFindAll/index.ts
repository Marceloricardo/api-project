import { DBTicketingRepository } from "../../repositores/implements/BDTicketingRepository";
import { TripsTicketingAllController } from "./TripsTicketingAllController";
import { TripsTicketingAllUseCase } from "./TripsTicketingAllUseCase";

const dbTicketingRepository = new DBTicketingRepository();

const tripsTicketingAllUseCase = new TripsTicketingAllUseCase(
    dbTicketingRepository
);

const tripsTicketingAllController = new TripsTicketingAllController(
    tripsTicketingAllUseCase
);

export { tripsTicketingAllUseCase, tripsTicketingAllController }