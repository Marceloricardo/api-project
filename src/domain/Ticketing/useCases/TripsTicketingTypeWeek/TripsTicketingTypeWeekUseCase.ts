import { IFindByTripsTypeWeek } from "domain/Ticketing/repositores/implements/interfaces";
import { ITicketingRepository } from "../../repositores/ITicketingRepository";
import { ITripsTicketingTypeWeekRequestDTO } from "./TripsTicketingTypeWeekDTO";

export class TripsTicketingTypeWeekUseCase {
    constructor(
        private ticketingRepository: ITicketingRepository,
    ) {
    }
    async execute(data: ITripsTicketingTypeWeekRequestDTO): Promise<IFindByTripsTypeWeek> {
        const findByTripsWeek = await this.ticketingRepository.findByTripsTypeWeek(
            data.type,
            data.dateStart,
            data.dateEnd,
            data.codLine, );


        return findByTripsWeek;
        

    }
}