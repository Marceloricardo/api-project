import { DBTicketingRepository } from "../../repositores/implements/BDTicketingRepository";
import { TripsTicketingTypeWeekController } from "./TripsTicketingTypeWeekController";
import { TripsTicketingTypeWeekUseCase } from "./TripsTicketingTypeWeekUseCase";

const dbTicketingRepository = new DBTicketingRepository();

const tripsTicketingTypeWeekUseCase = new TripsTicketingTypeWeekUseCase(
    dbTicketingRepository
);

const tripsTicketingTypeWeekController = new TripsTicketingTypeWeekController(
    tripsTicketingTypeWeekUseCase
);

export { tripsTicketingTypeWeekUseCase, tripsTicketingTypeWeekController }