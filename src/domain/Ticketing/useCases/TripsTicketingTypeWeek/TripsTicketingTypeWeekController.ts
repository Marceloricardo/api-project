import { Request, Response } from "express";
import { TripsTicketingTypeWeekUseCase } from "./TripsTicketingTypeWeekUseCase";

export class TripsTicketingTypeWeekController {
    constructor(
        private tripsTicketingTypeWeekUseCase: TripsTicketingTypeWeekUseCase,
    ) { }
    async handle(request: Request, response: Response): Promise<Response> {
 
        const { type,dateStart, dateEnd, codLine } = request.params;

            const tripsticketing = await this.tripsTicketingTypeWeekUseCase.execute({
                type,
                dateStart,
                dateEnd,
                codLine,
            });
                return response.status(200).json(tripsticketing);
         // #swagger.responses[200] = { description: 'dayweek and count' }
      
       
    }
}