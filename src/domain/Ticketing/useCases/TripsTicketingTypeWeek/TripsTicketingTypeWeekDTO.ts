interface ITripsTicketingTypeWeekRequestDTO {
    dateStart:string;
    dateEnd: string;
    codLine?: string
    type:string
}


export { ITripsTicketingTypeWeekRequestDTO };