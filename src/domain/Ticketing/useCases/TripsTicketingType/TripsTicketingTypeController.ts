import { Request, Response} from 'express';
import { TripsTicketingTypeUseCase } from './TripsTicketingTypeUseCase';




export class TripsTicketingTypeController {

constructor(
    private tripsTicketingTypeUseCase: TripsTicketingTypeUseCase,
)
{}
async handle(request:Request,response:Response):Promise<Response>{
    const { dateStart, dateEnd, codLine } = request.params;
    const tripsticketing = await this.tripsTicketingTypeUseCase.execute({
        dateEnd,
        dateStart,
        codLine
    });
    return response.status(200).json(tripsticketing);
    // #swagger.responses[200] = { description: 'passengers, entire, gratuity and student' }
}

}