import { DBTicketingRepository } from "../../repositores/implements/BDTicketingRepository";
import { TripsTicketingTypeController } from "./TripsTicketingTypeController";
import { TripsTicketingTypeUseCase } from "./TripsTicketingTypeUseCase";

const dbTicketingRepository = new DBTicketingRepository();

const tripsTicketingTypeUseCase = new TripsTicketingTypeUseCase(
    dbTicketingRepository
);

const tripsTicketingTypeController = new TripsTicketingTypeController(
    tripsTicketingTypeUseCase
);

export { tripsTicketingTypeUseCase, tripsTicketingTypeController }