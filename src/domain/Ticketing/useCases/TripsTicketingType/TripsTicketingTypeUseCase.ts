import { IFindByTripsType } from "../../../../domain/Ticketing/repositores/implements/interfaces";
import { ITicketingRepository } from "../../repositores/ITicketingRepository";
import { ITripsTicketingTypeRequestDTO } from "./TripsTicketingTypeDTO";

export class TripsTicketingTypeUseCase {
    constructor(
        private ticketingRepository: ITicketingRepository,
    ) {
    }
    async execute(data: ITripsTicketingTypeRequestDTO): Promise<IFindByTripsType> {
        const findByTripsType = await this.ticketingRepository.findByTripsType(data.dateStart,data.dateEnd,data.codLine);


        return findByTripsType;
        

    }
}