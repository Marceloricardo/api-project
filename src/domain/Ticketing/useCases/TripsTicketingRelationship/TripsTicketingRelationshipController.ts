import { Request, Response } from "express";
import { TripsTicketingRelationshipUseCase } from "./TripsTicketingRelationshipUseCase";

export class TripsTicketingRelationshipController {
    constructor(
        private tripsTicketingRelationshipUseCase: TripsTicketingRelationshipUseCase,
    ) { }
    async handle(request: Request, response: Response): Promise<Response> {
 
            const { dateStart, dateEnd, x,y,codLine } = request.params;
            const tripsticketing = await this.tripsTicketingRelationshipUseCase.execute({
                x,
                y,
                dateStart,
                dateEnd,
                codLine
               
            });
                return response.status(200).json(tripsticketing);
         // #swagger.responses[200] = { description: '[x, y and dateCollect' }
      
       
    }
}