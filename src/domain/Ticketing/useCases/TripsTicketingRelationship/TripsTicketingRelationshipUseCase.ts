import { IFindByTripsRelationship } from "domain/Ticketing/repositores/implements/interfaces";
import { ITicketingRepository } from "../../repositores/ITicketingRepository";
import { ITripsTicketingRelationshipRequestDTO } from "./TripsTicketingRelationshipDTO";

export class TripsTicketingRelationshipUseCase {
    constructor(
        private ticketingRepository: ITicketingRepository,
    ) {
    }
    async execute(data: ITripsTicketingRelationshipRequestDTO): Promise<IFindByTripsRelationship> {
        const findByTripsOperator = await this.ticketingRepository.findByTripsRelationship(
            data.x,data.y,
            data.dateStart,data.dateEnd,data.codLine
           );

        return findByTripsOperator;

    }
}