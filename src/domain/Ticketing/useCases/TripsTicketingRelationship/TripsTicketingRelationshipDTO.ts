interface ITripsTicketingRelationshipRequestDTO {
    dateStart:string;
    dateEnd: string;
    x:string;
    y:string;
    codLine?: string
}

export { ITripsTicketingRelationshipRequestDTO };