import { DBTicketingRepository } from "../../repositores/implements/BDTicketingRepository";
import { TripsTicketingRelationshipController } from "./TripsTicketingRelationshipController";
import { TripsTicketingRelationshipUseCase } from "./TripsTicketingRelationshipUseCase";

const dbTicketingRepository = new DBTicketingRepository();

const tripsTicketingRelationshipUseCase = new TripsTicketingRelationshipUseCase(
    dbTicketingRepository
);

const tripsTicketingRelationshipController = new TripsTicketingRelationshipController(
    tripsTicketingRelationshipUseCase
);

export { tripsTicketingRelationshipUseCase, tripsTicketingRelationshipController }