interface IAverageTripsTicketingTypeWeekRequestDTO {
    dateStart:string;
    dateEnd: string;
    codLine?: string
    type:string
}


export { IAverageTripsTicketingTypeWeekRequestDTO };