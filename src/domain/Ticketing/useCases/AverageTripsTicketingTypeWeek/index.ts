import { DBTicketingRepository } from "../../repositores/implements/BDTicketingRepository";
import { AverageTripsTicketingTypeWeekController } from "./AverageTripsTicketingTypeWeekController";
import { AverageTripsTicketingTypeWeekUseCase } from "./AverageTripsTicketingTypeWeekUseCase";

const dbTicketingRepository = new DBTicketingRepository();

const averageTripsTicketingTypeWeekUseCase = new AverageTripsTicketingTypeWeekUseCase(
    dbTicketingRepository
);

const averageTripsTicketingTypeWeekController = new AverageTripsTicketingTypeWeekController(
    averageTripsTicketingTypeWeekUseCase
);

export { averageTripsTicketingTypeWeekUseCase, averageTripsTicketingTypeWeekController }