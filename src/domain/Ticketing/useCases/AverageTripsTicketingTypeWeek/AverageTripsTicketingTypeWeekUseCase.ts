import { IFindAverageTripsWeeks } from "domain/Ticketing/repositores/implements/interfaces";
import { ITicketingRepository } from "../../repositores/ITicketingRepository";
import { IAverageTripsTicketingTypeWeekRequestDTO } from "./AverageTripsTicketingTypeWeekDTO";

export class AverageTripsTicketingTypeWeekUseCase {
    constructor(
        private ticketingRepository: ITicketingRepository,
    ) {
    }
    async execute(data: IAverageTripsTicketingTypeWeekRequestDTO): Promise<IFindAverageTripsWeeks> {
        const findByTripsWeek = await this.ticketingRepository.findAverageTripsWeek(
            data.dateStart,
            data.dateEnd,
            data.type,
            data.codLine, 
            );
        return findByTripsWeek;
        

    }
}