import { Request, Response } from "express";
import { AverageTripsTicketingTypeWeekUseCase } from "./AverageTripsTicketingTypeWeekUseCase";

export class AverageTripsTicketingTypeWeekController {
    constructor(
        private averageTripsTicketingTypeWeekUseCase: AverageTripsTicketingTypeWeekUseCase,
    ) { }
    async handle(request: Request, response: Response): Promise<Response> {
 
        const { type,dateStart, dateEnd, codLine } = request.params;
            const tripsticketing = await this.averageTripsTicketingTypeWeekUseCase.execute({
                dateStart:dateStart,
                dateEnd:dateEnd,
                codLine:codLine,
                type:type,
            });
                return response.status(200).json(tripsticketing);
      
       
    }
}