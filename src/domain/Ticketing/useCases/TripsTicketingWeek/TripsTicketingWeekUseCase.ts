import { IFindByTripsWeeks } from "domain/Ticketing/repositores/implements/interfaces";
import { ITicketingRepository } from "../../repositores/ITicketingRepository";
import { ITripsTicketingWeekRequestDTO } from "./TripsTicketingWeekDTO";

export class TripsTicketingWeekUseCase {
    constructor(
        private ticketingRepository: ITicketingRepository,
    ) {
    }
    async execute(data: ITripsTicketingWeekRequestDTO): Promise<IFindByTripsWeeks> {
        const findByTripsWeek = await this.ticketingRepository.findByTripsWeek(data.dateStart,data.dateEnd,data.codLine);


        return findByTripsWeek;
        

    }
}