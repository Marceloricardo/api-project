interface ITripsTicketingWeekRequestDTO {
    dateStart:string;
    dateEnd: string;
    codLine?: string
}


export { ITripsTicketingWeekRequestDTO };