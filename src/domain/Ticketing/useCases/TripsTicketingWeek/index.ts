import { DBTicketingRepository } from "../../repositores/implements/BDTicketingRepository";
import { TripsTicketingWeekController } from "./TripsTicketingWeekController";
import { TripsTicketingWeekUseCase } from "./TripsTicketingWeekUseCase";

const dbTicketingRepository = new DBTicketingRepository();

const tripsTicketingWeekUseCase = new TripsTicketingWeekUseCase(
    dbTicketingRepository
);

const tripsTicketingWeekController = new TripsTicketingWeekController(
    tripsTicketingWeekUseCase
);

export { tripsTicketingWeekUseCase, tripsTicketingWeekController }