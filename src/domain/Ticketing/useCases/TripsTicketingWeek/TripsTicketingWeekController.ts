import { Request, Response } from "express";
import { TripsTicketingWeekUseCase } from "./TripsTicketingWeekUseCase";

export class TripsTicketingWeekController {
    constructor(
        private tripsTicketingWeekUseCase: TripsTicketingWeekUseCase,
    ) { }
    async handle(request: Request, response: Response): Promise<Response> {
 
        const { dateStart, dateEnd, codLine } = request.params;

            const tripsticketing = await this.tripsTicketingWeekUseCase.execute({
                dateStart,
                dateEnd,
                codLine
            });
                return response.status(200).json(tripsticketing);
         // #swagger.responses[200] = { description: 'dayWeek and count' }
      
       
    }
}