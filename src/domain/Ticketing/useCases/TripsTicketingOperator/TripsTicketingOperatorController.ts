import { Request, Response } from "express";
import { TripsTicketingOperatorUseCase } from "./TripsTicketingOperatorUseCase";

export class TripsTicketingOperatorController {
    constructor(
        private tripsTicketingOperatorUseCase: TripsTicketingOperatorUseCase,
    ) { }
    async handle(request: Request, response: Response): Promise<Response> {
 
        const { dateStart, dateEnd, codLine } = request.params;

            const tripsticketing = await this.tripsTicketingOperatorUseCase.execute({
                dateStart,
                dateEnd,
                codLine
            });
                return response.status(200).json(tripsticketing);
                // #swagger.responses[200] = { description: '[nameOperator and count]' }

      
       
    }
}