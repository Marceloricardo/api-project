import { IFindByTripsOperator } from "domain/Ticketing/repositores/implements/interfaces";
import { ITicketingRepository } from "../../repositores/ITicketingRepository";
import { ITripsTicketingOperatorRequestDTO } from "./TripsTicketingOperatorDTO";

export class TripsTicketingOperatorUseCase {
    constructor(
        private ticketingRepository: ITicketingRepository,
    ) {
    }
    async execute(data: ITripsTicketingOperatorRequestDTO): Promise<IFindByTripsOperator> {
        const findByTripsOperator = await this.ticketingRepository.findByTripsOperator(data.dateStart,data.dateEnd,data.codLine);


        return findByTripsOperator;
        

    }
}