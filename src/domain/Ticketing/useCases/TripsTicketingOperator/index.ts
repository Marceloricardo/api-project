import { DBTicketingRepository } from "../../repositores/implements/BDTicketingRepository";
import { TripsTicketingOperatorController } from "./TripsTicketingOperatorController";
import { TripsTicketingOperatorUseCase } from "./TripsTicketingOperatorUseCase";

const dbTicketingRepository = new DBTicketingRepository();

const tripsTicketingOperatorUseCase = new TripsTicketingOperatorUseCase(
    dbTicketingRepository
);

const tripsTicketingOperatorController = new TripsTicketingOperatorController(
    tripsTicketingOperatorUseCase
);

export { tripsTicketingOperatorUseCase, tripsTicketingOperatorController }