interface ITripsTicketingOperatorRequestDTO {
    dateStart:string;
    dateEnd: string;
    codLine?: string
}


export { ITripsTicketingOperatorRequestDTO };