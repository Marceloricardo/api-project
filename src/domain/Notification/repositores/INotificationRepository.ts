import amqplib from   'amqplib';
export interface INotificationRepository {
    connect():Promise<amqplib.Channel>;
    createQueue(channel:amqplib.Channel,queue:string):Promise<amqplib.Channel>
}