import { ConsumeNotificationUseCase } from "./ConsumeNotificationUseCase";

export class ConsumerNotificationController {
    constructor(
        private consumeNotificationUseCase: ConsumeNotificationUseCase,
    ) { }
    async handle(): Promise<void> {
const data = {
    queue:'admin',
}
     await this.consumeNotificationUseCase.execute(data);
      
    }
}