interface IConsumerNotificationRequestDTO {
    channel?: string;
    queue: string;
}


export { IConsumerNotificationRequestDTO };