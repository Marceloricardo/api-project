import { SVNotificationRepository } from "../../../../domain/Notification/repositores/implements/SVNotificationRepository";
import { ConsumerNotificationController } from "./ConsumerNotificationController";
import { ConsumeNotificationUseCase } from "./ConsumeNotificationUseCase";

const svNotificationRepository = new SVNotificationRepository();

const consumeNotificationUseCase = new ConsumeNotificationUseCase(
    svNotificationRepository
);

const consumerNotificationController = new ConsumerNotificationController(
    consumeNotificationUseCase
);

export { consumeNotificationUseCase, consumerNotificationController }