import { IConsumerNotificationRequestDTO } from "./ConsumerNotificationRequestDTO";
import { INotificationRepository } from "../../repositores/INotificationRepository";
export class ConsumeNotificationUseCase {
    constructor(
        private notificationRepository: INotificationRepository

    ) {

    }
    async execute(data: IConsumerNotificationRequestDTO): Promise<void> {

 const conn= await this.notificationRepository.connect();
 
 const channel= await this.notificationRepository.createQueue(conn,data.queue)
 channel.consume(data.queue,function(msg) {
    if (msg !== null) {
      console.log(msg.content.toString());
      channel.ack(msg);
    }
  });
    }
}