import { User } from "../../../../entities/User";
import { IUserRepository } from "../IUserRepository";
import UserModel from '../../../../models/user.model';
import { HelperValidateEmail } from "../../../../helper/HelperValidateEmail";

export class DBUserRepository implements IUserRepository {
    async findByEmail(email: string): Promise<User | undefined> {
        const helperValidateEmail= new HelperValidateEmail();
        const emailIsValid = await helperValidateEmail.validateEmail(email);
        if(!emailIsValid){
            throw new Error("Invalid Email Format");
        }
        const user = await UserModel.findOne({ email: email }).exec();
        if (user) {
            return user;
        }
        return undefined;
    }
    async save(user: User): Promise<void> {

        await UserModel.create(user);
    }


}