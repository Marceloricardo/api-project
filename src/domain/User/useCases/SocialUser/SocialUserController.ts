import { Request, Response } from "express";
import { SocialUserUseCase } from "./SocialUserUseCase";

export class SocialUserController {
    constructor(
        private socialUserUseCase: SocialUserUseCase,
    ) { }
    async handle(request: Request,response:Response): Promise<Response> {
        const name = request.body.user.name;
        const email = request.body.user.email;
        const { phone, provider } = request.body.user;
        const loginsocial = await this.socialUserUseCase.execute({
            name,
            email,
            phone,
            provider
        })
        if (loginsocial) {
            return response.status(200).json(loginsocial);
            // #swagger.responses[200] = { description: 'token' }
        }
        return response.status(401).json({
            message: 'Unauthorized'

        })
            // #swagger.responses[401] = { description: 'Unauthorized' }


    }


}