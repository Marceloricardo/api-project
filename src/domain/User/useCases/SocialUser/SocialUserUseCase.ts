import { ServiceEncryptPassword } from "../../../../services/password/cryptography/ServiceEncryptPassword";
import { User } from "../../../../entities/User";
import { IUserRepository } from "../../repositores/IUserRepository";
import { ISocialUserRequestDTO } from "./SocialUserDTO";
import { ServiceGeneratePassword } from "../../../../services/password/ServiceGeneratePassword";
import { ServiceAuthUser } from "../../../../services/auth/ServiceAuthUser";

export class SocialUserUseCase {
    constructor(
        private userRepository: IUserRepository

    ) {

    }
    async execute(data: ISocialUserRequestDTO): Promise<string> {
        const userAlreadyExists =
            await this.userRepository.findByEmail(data.email);
        const generatetoken = new ServiceAuthUser();



        if (userAlreadyExists) {
            if (userAlreadyExists.provider != data.provider) {
                throw new Error('Invalid Authentication Method');
            }
            const dataToken = { id: userAlreadyExists._id, email: userAlreadyExists.email,name:userAlreadyExists.name }
            const token = await generatetoken.generateToken(dataToken);
            return token;
        }

        if (!userAlreadyExists) {
            const serviceGeneratePassword = new ServiceGeneratePassword();
            let password = await serviceGeneratePassword.generatePassword();
            //usa o service de criptografia do bcrypt 
            const serviceEncryptPassword = new ServiceEncryptPassword();
            await serviceEncryptPassword.encryptPassword(password).then((encryptedPassword) => {
                password = encryptedPassword
            })
            const user = new User(data);
            user.password = password;
            await this.userRepository.save(user);
            const dataToken = { id: user._id, email: user.email,name:user.name }
            const token = await generatetoken.generateToken(dataToken);
            return token;
        }
        throw new Error('Incorrect Credentials');
    }
}