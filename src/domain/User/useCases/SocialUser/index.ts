import { DBUserRepository } from "../../repositores/implements/BDUserRepository";
import { SocialUserController } from "./SocialUserController";
import { SocialUserUseCase } from "./SocialUserUseCase";

const dbUserRepository = new DBUserRepository();

const socialUserUseCase = new SocialUserUseCase(
    dbUserRepository
);

const socialUserController = new SocialUserController(
    socialUserUseCase
);

export { socialUserUseCase, socialUserController }