export interface ISocialUserRequestDTO {
    name: string;
    email: string;
    phone: string;
    provider: string;
}