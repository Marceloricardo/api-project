interface ILoginUserRequestDTO {
    email: string;
    password: string;
    provider: string;
}


export { ILoginUserRequestDTO };