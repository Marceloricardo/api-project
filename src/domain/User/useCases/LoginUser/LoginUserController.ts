import { Request, Response } from "express";
import { LoginUserUseCase } from "./LoginUserUseCase";

export class LoginUserController {
    constructor(
        private LoginUserUseCase: LoginUserUseCase,
    ) { }
    async handle(request: Request, response: Response): Promise<Response> {
        const { email, password, provider } = request.body.user;

        const loginuser = await this.LoginUserUseCase.execute({
            email,
            password,
            provider
        })
        if (loginuser) {
            return response.status(200).json(loginuser);
     // #swagger.responses[200] = { description: 'token' }
        }
        return response.status(401).json({
            message: 'Unauthorized'

        });
                    // #swagger.responses[401] = { description: 'Unauthorized' }

    }
}