import { DBUserRepository } from "../../../../domain/User/repositores/implements/BDUserRepository";
import { LoginUserController } from "./LoginUserController";
import { LoginUserUseCase } from "./LoginUserUseCase";

const dbUserRepository = new DBUserRepository();

const loginUserUseCase = new LoginUserUseCase(
    dbUserRepository
);

const loginUserController = new LoginUserController(
    loginUserUseCase
);

export { loginUserUseCase, loginUserController }