import { ServiceAuthUser } from "../../../../services/auth/ServiceAuthUser";
import { ServiceVerifyEncryptedPassword } from "../../../../services/password/cryptography/ServiceVerifyEncryptedPassword";
import { IUserRepository } from "../../repositores/IUserRepository";
import { ILoginUserRequestDTO } from "./LoginUserDTO";

export class LoginUserUseCase {
    constructor(
        private userRepository: IUserRepository,
    ) {
    }
    async execute(data: ILoginUserRequestDTO): Promise<string> {

        const userAlreadyExists = await this.userRepository.findByEmail(data.email);

        if (!userAlreadyExists) {

            throw new Error('Incorrect Credentials');
        }

        //usa o service de criptografia do bcrypt 
        const serviceVerifyEncryptedPassword = new ServiceVerifyEncryptedPassword();
        const userVerify = await serviceVerifyEncryptedPassword.verifyencryptPassword(data.password, userAlreadyExists.password ? userAlreadyExists.password : '')
        const generatetoken = new ServiceAuthUser();
        const dataToken = { id: userAlreadyExists._id, email: userAlreadyExists.email,name:userAlreadyExists.name }
        const token = await generatetoken.generateToken(dataToken);
        if (userVerify) {

            return token;
        }
        throw new Error('Incorrect Credentials');

    }
}