import { ServiceEncryptPassword } from "../../../../services/password/cryptography/ServiceEncryptPassword";
import { User } from "../../../../entities/User";
import { IUserRepository } from "../../repositores/IUserRepository";
import { ICreateUserRequestDTO } from "./CreateUserDTO";

export class CreateUserUseCase {
    constructor(
        private userRepository: IUserRepository

    ) {

    }
    async execute(data: ICreateUserRequestDTO): Promise<void> {
        
        const userAlreadyExists =
            await this.userRepository.findByEmail(data.email);

        if (userAlreadyExists) {
            throw new Error('User already exists.');
        }

        //usa o service de criptografia do bcrypt 
        const serviceEncryptPassword = new ServiceEncryptPassword();
        await serviceEncryptPassword.encryptPassword(data.password).then((encryptedPassword) => {
            data.password = encryptedPassword
        })
        const user = new User(data);
        await this.userRepository.save(user);
    }
}