import { Request, Response } from "express";
import { CreateUserUseCase } from "./CreateUserUseCase";

export class CreateUserController {
    constructor(
        private createUserUseCase: CreateUserUseCase,
    ) { }
    async handle(request: Request, response: Response): Promise<Response> {
        const name = request.body.user.name;
        const email = request.body.user.email;
        const { phone, provider, password } = request.body.user;
        await this.createUserUseCase.execute({
            name,
            email,
            password,
            phone,
            provider
        })
        return response.status(201).send();
        // #swagger.responses[201]

    }
}