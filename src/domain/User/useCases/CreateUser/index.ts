import { DBUserRepository } from "../../../../domain/User/repositores/implements/BDUserRepository";
import { CreateUserController } from "./CreateUserController";
import { CreateUserUseCase } from "./CreateUserUseCase";

const dbUserRepository = new DBUserRepository();

const createUserUseCase = new CreateUserUseCase(
    dbUserRepository
);

const createUserController = new CreateUserController(
    createUserUseCase
);

export { createUserUseCase, createUserController }