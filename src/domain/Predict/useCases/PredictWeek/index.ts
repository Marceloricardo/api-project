import { DBTicketingRepository } from "../../../../domain/Ticketing/repositores/implements/BDTicketingRepository";
import { AverageTripsTicketingTypeWeekUseCase } from "../../../../domain/Ticketing/useCases/AverageTripsTicketingTypeWeek/AverageTripsTicketingTypeWeekUseCase";
import { DBPredictRepository } from "../../repositores/implements/BDPredictRepository";
import { PredictWeekController } from "./PredictWeekController";
import { PredictWeekUseCase } from "./PredictWeekUseCase";

const dbTicketingRepository = new DBTicketingRepository();
const averageTripsTicketingTypeWeekUseCase = new AverageTripsTicketingTypeWeekUseCase(
    dbTicketingRepository
);

const dbPredictRepository = new DBPredictRepository();
const predictgWeekUseCase = new PredictWeekUseCase(
    dbPredictRepository,
);

const predictWeekController = new PredictWeekController(
    predictgWeekUseCase,
    averageTripsTicketingTypeWeekUseCase
);

export { predictgWeekUseCase, predictWeekController }