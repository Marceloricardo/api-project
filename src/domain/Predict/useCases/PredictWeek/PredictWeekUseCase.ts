import { IFindByTripsWeeks } from "domain/Ticketing/repositores/implements/interfaces";
import { IPredictRepository } from "../../repositores/IPredictRepository";
import { IPredictWeekRequestDTO } from "./PredictWeekDTO";

export class PredictWeekUseCase {
    constructor(
        private predictRepository: IPredictRepository,
    ) {
    }
    async execute(data: IPredictWeekRequestDTO): Promise<IFindByTripsWeeks> {
        
        const predictsWeek = await this.predictRepository.findPredict(data.type);


        return predictsWeek;
        

    }
}