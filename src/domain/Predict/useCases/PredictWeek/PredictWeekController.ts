import { AverageTripsTicketingTypeWeekUseCase } from "../../../../domain/Ticketing/useCases/AverageTripsTicketingTypeWeek/AverageTripsTicketingTypeWeekUseCase";
import { Request, Response } from "express";
import { PredictWeekUseCase } from "./PredictWeekUseCase";
import { HelperValidateDate } from "../../../../helper/HelperValidateDate";

export class PredictWeekController {
    constructor(
        private predictWeekUseCase: PredictWeekUseCase,
        public averageTripsTicketingTypeWeekUseCase : AverageTripsTicketingTypeWeekUseCase

    ) { }
    async handle(request: Request, response: Response): Promise<Response> {
            const { type,codLine } = request.params;
            const helperValidateDate = new HelperValidateDate();
            const {weekstart,weekend} = await helperValidateDate.validateDatePredict();
            const dateStart = weekstart;
            const dateEnd = weekend;

            const tripsWeek = await this.averageTripsTicketingTypeWeekUseCase.execute({
                dateStart:dateStart,
                dateEnd:dateEnd,
                type:type,
                codLine:codLine,
            });
            const predictweek = await this.predictWeekUseCase.execute({
                type
            });

for(let i = 0;i<tripsWeek.length;i++){
tripsWeek[i].count = predictweek[0].constpredict+predictweek[0].x1predict*tripsWeek[i].count
}
            
                return response.status(200).json(tripsWeek);
         // #swagger.responses[200] = { description: 'dayWeek and count' }
      
       
    }
}