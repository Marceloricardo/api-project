import { IPredictRepository } from "../IPredictRepository";
import { IPredictWeek } from "./interfaces";
import PredictModel from  "../../../../models/prediction.model"
export class DBPredictRepository implements IPredictRepository {
    async findPredict(type:string): Promise<IPredictWeek|any> {
        const predict = await PredictModel.find({typepredict:type}).exec();
        return predict;
        }
}