//import { Ticketing } from "../../../entities/Ticketing";

import { IPredictWeek } from "./implements/interfaces";

export interface IPredictRepository {
    findPredict(type:string, codLine?: string): Promise<IPredictWeek|any>;
   
}