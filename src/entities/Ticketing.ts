import { v4 as uuid_v4 } from "uuid";
export class Ticketing {
    public readonly _id?: string;
    public dateCollect: string;
    public nameOperator: string;
    public passengers: number;
    public entire: number;
    public gratuity: number;
    public codOperator: number;
    public student: number;
    public dayWeek: string;
    public nameLine: string;

    constructor(props: Omit<Ticketing, '_id'>, _id?: string) {
        Object.assign(this, props);
        if (!_id) {
            this._id = uuid_v4();
        }
    }

}