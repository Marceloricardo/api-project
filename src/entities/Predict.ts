import { v4 as uuid_v4 } from "uuid";
export class Predict {
    public readonly _id?: string;
    public constpredict: number;
    public x1predict: number;
    public typepredict: string

    constructor(props: Omit<Predict, '_id'>, _id?: string) {
        Object.assign(this, props);
        if (!_id) {
            this._id = uuid_v4();
        }
    }

}