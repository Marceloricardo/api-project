import { v4 as uuid_v4 } from "uuid";
export class User {
    public readonly _id?: string;
    public name: string;
    public email: string;
    public password?: string;
    public phone?: string;
    public provider?: string;

    constructor(props: Omit<User, '_id'>, _id?: string) {
        Object.assign(this, props);
        if (!_id) {
            this._id = uuid_v4();
        }
    }

}