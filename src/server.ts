import { app } from "./app";
import log from './logger/log';
app.listen(process.env.PORT || 3333, () => log.info('Serve rodando porta 3333'));
