import moment from "moment";
interface IValidateDate{
    datestart:string,
    dateend:string
}
interface IValidateDateWeek{
    weekstart:string,
    weekend:string
}
class HelperValidateDate {
    async validateDate(dateStart:string, dateEnd:string):Promise<IValidateDate>{
        const format = 'YYYY-MM-DD';
        const hourStart = ' 00:00:00';
        const hourEnd = ' 23:59:59';
        const validDateStart = moment(dateStart,format,true);
        const validDateEnd = moment(dateEnd,format,true);
        if(!dateStart && !dateEnd){
            const datestart = moment().subtract(1, 'months').format(format);
            const dateend = moment().format(format);
            return {
                datestart:datestart + hourStart,
                dateend: dateend + hourEnd
            }
        }
        else if(!validDateStart.isValid() || !validDateEnd.isValid()){
            throw new Error("Invalid Date Format");
        }
        return {
            datestart:dateStart + hourStart ,
            dateend:dateEnd + hourEnd
        }
    }
    async validateDatePredict():Promise<IValidateDateWeek>{
//2021-08-31 data mais recente na base
const format = 'YYYY-MM-DD';
const dateStart = moment("2021-03-01").day(0);
const weekStart = dateStart.subtract(21, 'days').format(format);
const weekEnd = moment("2021-03-01").day(6).format(format); 
return {
    weekstart:weekStart,
    weekend:weekEnd,
}

    }

}
export {HelperValidateDate}