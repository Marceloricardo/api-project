class HelperValidateEmail {
    async validateEmail(email:string):Promise<boolean>{
        const re = /\S+@\S+\.\S+/;
        return re.test(email);
    }
}

export {HelperValidateEmail}