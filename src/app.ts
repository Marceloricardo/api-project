import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import 'express-async-errors';
import swaggerUi from  'swagger-ui-express';
import swaggerFile from  './swagger_output.json';
import { routerSocialAuth } from './routes/routesSocialAuth';
import { routerUserAuth } from './routes/routesUserAuth';
import mongo from './database/index';
import { logger } from './logger';
import { errorHandling } from './middlewares/middlewareserrors';
import cors from 'cors';
//import { consumerNotificationController } from './domain/Notification/useCases/ConsumeNotification';
import { routerBase } from './routes/routesBase';
import { routerTrips } from './routes/routesTrips';
import { routerPredict } from './routes/routesPredict';
const app = express();
//consumerNotificationController.handle();
app.use(express.json());
app.use(logger);
app.use(cors())
mongo;
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile))
app.use([ routerSocialAuth, routerUserAuth,routerBase, routerTrips,routerPredict ]);
app.use(errorHandling);

export { app }