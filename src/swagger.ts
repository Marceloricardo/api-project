// eslint-disable-next-line @typescript-eslint/no-var-requires
const swaggerAutogen = require('swagger-autogen')()

const outputFile = 'src/swagger_output.json'
const endpointsFiles = [ 'src/routes/routesSocialAuth.ts','src/routes/routesUserAuth.ts',
'src/routes/routesTrips.ts','src/routes/routesPredict.ts' ]
const doc = {
    swagger: "2.0",
    info: {
        version: "1.0.0",
        title: "DASHBUS",
        description: "Documentação da API de Consumo de Dados do DASHBUS (Sistema dashboard para análise de dados do STP de Teresina)"
    },
    host: "dashbus-api.herokuapp.com",
    basePath: "/",
    schemes: [ 'http', 'https' ],
    consumes: [ 'application/json' ],
    produces: [ 'application/json' ],
    securityDefinitions:{
  Bearer:{
    type: "apiKey",
  name: "Authorization",
    in: "header"
    },
    AcessToken:{
        type:'apyKey',
        name:'acesstoken',
        in:'header'
    }
},
tools: {
  React: 2.0,
},
definitions: {
  AddUser: {
      $name: "Usuario",
      $email:"user@email.com",
      $password:"54a5s151dsad"
  },
  AuthSocial: {
    $name: "Usuario",
    $email:"user@email.com",
},
LoginUser: {
  $email:"user@email.com",
  $password:"54a5s151dsad"
},
}
};
swaggerAutogen(outputFile, endpointsFiles,doc).then(() => {
    require('./server.ts')
})