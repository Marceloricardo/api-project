import { User } from '../entities/User';
import { Schema, model, Document } from 'mongoose';

export interface UserInterface extends Document {
    name: string;
    email: string;
    password: string;
    phone: string;

}
const UserSchema = new Schema({

    name: {
        type: String,
        require: true,
    },
    email: {
        type: String,
        require: true,
    },
    password: {
        type: String,
        require: true,
        select: true,
    },
    provider: {
        type: String,
        require: false,
    },
    phone: {
        type: String,
        require: false,
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    _id: {
        type: String
    }
});
UserSchema.loadClass(User);

const UserModel = model<UserInterface>('User', UserSchema);
export default UserModel;