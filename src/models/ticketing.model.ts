import { Schema, model, Document } from 'mongoose';
import { Ticketing } from '../entities/Ticketing';

export interface TicketingInterface extends Document {
     dateCollect: string;
     nameOperator: string;
     passengers: number;
     entire: number;
     gratuity: number;
     codOperator: number;
     student: number;
     dayWeek: string;
     nameLine: string;
}
const TicketingSchema = new Schema({

    dateCollect: {
        type: String,
        require: true,
    },
    nameOperator: {
        type: String,
        require: true,
    },
    passengers: {
        type: Number,
        require: true,
        select: true,
    },
    entire: {
        type: Number,
        require: true,
        select: true,
    },
    gratuity: {
        type: Number,
        require: true,
        select: true,
    },
    codOperator: {
        type: Number,
        require: true,
        select: true,
    },
    student: {
        type: Number,
        require: true,
        select: true,
    },
    dayWeek: {
        type: String,
        require: false,
    },
    nameLine: {
        type: String,
        require: false,
    },
    _id: {
        type: String
    }
});
TicketingSchema.loadClass(Ticketing);

const TicketinModel = model<TicketingInterface>('Ticketing', TicketingSchema);
export default TicketinModel;