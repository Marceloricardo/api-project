import { Schema, model, Document } from 'mongoose';
import { Predict } from '../entities/Predict';

export interface PredictInterface extends Document {
    constpredict: number,
    x1predict: number,
    typepredict: string
}
const PredictSchema = new Schema({

    constpredict: {
        type: Number,
        require: true,
    },
    x1predict: {
        type: Number,
        require: true,
    },
    typepredict: {
        type: String,
        require: true,
    },
    
    _id: {
        type: String
    }
});
PredictSchema.loadClass(Predict);
const PredictModel = model<PredictInterface>('Predict', PredictSchema);
export default PredictModel;