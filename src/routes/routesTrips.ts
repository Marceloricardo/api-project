
import { tripsTicketingOperatorController } from "../domain/Ticketing/useCases/TripsTicketingOperator";
import { Router } from "express";
import { tripsTicketingTypeController } from "../domain/Ticketing/useCases/TripsTicketingType";
import { tripsTicketingAllController } from "../domain/Ticketing/useCases/TripsTicketingFindAll";
import { tripsTicketingRelationshipController } from "../domain/Ticketing/useCases/TripsTicketingRelationship";
import { tripsTicketingWeekController } from "../domain/Ticketing/useCases/TripsTicketingWeek";
import { tripsTicketingTypeWeekController } from "../domain/Ticketing/useCases/TripsTicketingTypeWeek";
import { validToken } from "../middlewares/middlewaresauthuser";

const routerTrips = Router()
// viagem por empresas
routerTrips.get('/ticketing/trips/operator/:dateStart?/:dateEnd?/:codLine?',async (request, response) => {
      // #swagger.tags = ['Trips/Operator']
        // #swagger.description = 'Quantidade de viagens por empresas'
          /* 
          #swagger.parameters['acessToken'] = { in: 'header', description: 'Token' } 
          #swagger.parameters['dateStart?'] = { in: 'path', description: 'Data inicial' , required: false } 
          #swagger.parameters['dateEnd?'] = { in: 'path', description: 'Data final',  required: false } 
          #swagger.parameters['codLine?'] = { in: 'path', description: 'Código da linha' , required: false } 
          */
         
    return await tripsTicketingOperatorController.handle(request,response);
    });
      
// Viagens por tipo de passagem
routerTrips.get('/ticketing/trips/types/:dateStart?/:dateEnd?/:codLine?',async (request, response) => {
    // #swagger.tags = ['Trips/Types']
        // #swagger.description = 'Viagens por tipo de passagem vendida'
          /* 
          #swagger.parameters['acessToken'] = { in: 'header', description: 'Token' } 
          #swagger.parameters['dateStart?'] = { in: 'path', description: 'Data inicial' , required: false } 
          #swagger.parameters['dateEnd?'] = { in: 'path', description: 'Data final',  required: false } 
          #swagger.parameters['codLine?'] = { in: 'path', description: 'Código da linha' , required: false } 
          */
    return await tripsTicketingTypeController.handle(request,response);
    });
// Quantidade de viagens 
routerTrips.get('/ticketing/trips/count/:dateStart?/:dateEnd?/:codLine?',async (request, response) => {
     // #swagger.tags = ['Trips/Count']
        // #swagger.description = 'Quantidade de viagens '
          /* 
          #swagger.parameters['acessToken'] = { in: 'header', description: 'Token' } 
          #swagger.parameters['dateStart?'] = { in: 'path', description: 'Data inicial' , required: false } 
          #swagger.parameters['dateEnd?'] = { in: 'path', description: 'Data final',  required: false } 
          #swagger.parameters['codLine?'] = { in: 'path', description: 'Código da linha' , required: false } 
          */
    return await tripsTicketingAllController.handle(request,response);
    });
    
    // Correlação de tipo de passagem
routerTrips.get('/ticketing/trips/relationship/:x/:y/:dateStart?/:dateEnd?/:codLine?',async (request, response) => {
     // #swagger.tags = ['Trips/Relationship']
        // #swagger.description = 'Correlação de tipo de passagem'
          /* 
          #swagger.parameters['acessToken'] = { in: 'header', description: 'Token' } 
          #swagger.parameters['y'] = { in: 'path', description: 'Coluna para o eixo Y' , required: true } 
          #swagger.parameters['x'] = { in: 'path', description: 'Coluna para o eixo X',  required: true } 
          #swagger.parameters['dateStart?'] = { in: 'path', description: 'Data inicial' , required: false } 
          #swagger.parameters['dateEnd?'] = { in: 'path', description: 'Data final',  required: false } 
          #swagger.parameters['codLine?'] = { in: 'path', description: 'Código da linha' , required: false } 
          */
    return await tripsTicketingRelationshipController.handle(request,response);
    });

    // Quantidade de viagens por dia da semana
routerTrips.get('/ticketing/trips/week/:dateStart?/:dateEnd?/:codLine?',async (request, response) => {
      // #swagger.tags = ['Trips/Week']
        // #swagger.description = ' Quantidade de viagens por dia da semana'
          /* 
          #swagger.parameters['acessToken'] = { in: 'header', description: 'Token' } 
          #swagger.parameters['dateStart?'] = { in: 'path', description: 'Data inicial' , required: false } 
          #swagger.parameters['dateEnd?'] = { in: 'path', description: 'Data final',  required: false } 
          #swagger.parameters['codLine?'] = { in: 'path', description: 'Código da linha' , required: false } 
          */
    return await tripsTicketingWeekController.handle(request,response);
    });


    // Quantidade de passagens por tipo e por dia da semana
routerTrips.get('/ticketing/trips/type/week/:type/:dateStart?/:dateEnd?/:codLine?',async (request, response) => {
    // #swagger.tags = ['Trips/Type/Week']
        // #swagger.description = 'Quantidade de passagens por tipo e por dia da semana'
          /* 
          #swagger.parameters['acessToken'] = { in: 'header', description: 'Token' } 
          #swagger.parameters['type'] = { in: 'path', description: 'Tipo de passagem' , required: true } 
          #swagger.parameters['dateStart?'] = { in: 'path', description: 'Data inicial' , required: false } 
          #swagger.parameters['dateEnd?'] = { in: 'path', description: 'Data final',  required: false } 
          #swagger.parameters['codLine?'] = { in: 'path', description: 'Código da linha' , required: false } 
          */
    return await tripsTicketingTypeWeekController.handle(request,response);
    });
            
export { routerTrips }