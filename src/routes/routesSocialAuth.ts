
import { Router } from "express";
import { socialUserController, } from "../domain/User/useCases/SocialUser";

const routerSocialAuth = Router()
//auth facebook
routerSocialAuth.post('/auth/facebook',async (request, response) => {
    // #swagger.tags = ['Auth/Facebook']
        // #swagger.description = 'Cadastro via Facebook'
          // #swagger.description = 'Cadastro via Facebook'
          /* #swagger.parameters['acessToken'] = { in: 'header', description: 'Acess Token FACEBOOK' } */
                       /*
 #swagger.parameters['Auth Facebook'] = {
   description: 'Dados do usuario.',
   type: 'object',
   required: true,
   in: 'body',
   schema: { $ref: "#/definitions/AuthSocial" }
  }
*/
    request.body.user = request.body;
    request.body.user.provider = 'facebook'
        return await socialUserController.handle(request,response);
    });

//auth google
routerSocialAuth.post('/auth/google',async (request, response) => {
    // #swagger.tags = ['Auth/Google']
        // #swagger.description = 'Cadastro via Google'
         /* #swagger.parameters['acessToken'] = { in: 'header', description: 'Acess Token GOOGLE' } */
                  /*
 #swagger.parameters['Auth Google'] = {
   description: 'Dados do usuario.',
   type: 'object',
   required: true,
   in: 'body',
   schema: { $ref: "#/definitions/AuthSocial" }
  }
*/
request.body.user = request.body;
request.body.user.provider = 'google'
    return await socialUserController.handle(request,response);
 
});
export { routerSocialAuth }