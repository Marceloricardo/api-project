
import { predictWeekController } from "../domain/Predict/useCases/PredictWeek";
import { Router } from "express";
import { validToken } from "../middlewares/middlewaresauthuser";
const routerPredict = Router()
// Previsão de quantidade média de passageiros por viagem
routerPredict.get('/predict/trips/week/:type/:codLine?',async (request, response) => {
     // #swagger.tags = ['Predict/Trips/Type']
        // #swagger.description = 'Previsão de quantidade média de passageiros por viagem'
          /* 
          #swagger.parameters['acessToken'] = { in: 'header', description: 'Token' } 
          #swagger.parameters['type'] = { in: 'path', description: 'A previsão pode ser feita considerando 2 variaveis: Quantidade de gratuidade e quantidade de inteiras' , required: true } 
          #swagger.parameters['codLine?'] = { in: 'path', description: 'Código da linha' , required: false } 
          */
    return await  predictWeekController.handle(request,response);
    });

export { routerPredict }