import { createUserController } from "../domain/User/useCases/CreateUser";
import { loginUserController } from "../domain/User/useCases/LoginUser";
import { Router } from "express";
const routerUserAuth = Router()


routerUserAuth.post('/register/local', (request, response) => {
// #swagger.tags = ['Register/Local']
// #swagger.description = 'Cadastro via email e senha'
                    /*
 #swagger.parameters['Cadastro'] = {
   description: 'Dados do usuario.',
   type: 'object',
   required: true,
   in: 'body',
   schema: { $ref: "#/definitions/AddUser" }
  }
*/
 
    request.body.user = request.body
    request.body.user.provider = 'local'
    return createUserController.handle(request, response);
});
routerUserAuth.post('/auth/local', (request, response) => {
    // #swagger.tags = ['Login/Local']
// #swagger.description = 'Login via email e senha'
                      /*
 #swagger.parameters['Login'] = {
   description: 'Dados do usuario.',
   type: 'object',
   required: true,
   in: 'body',
   schema: { $ref: "#/definitions/LoginUser" }
  }
*/
    request.body.user = request.body
    request.body.user.provider = 'local'
    return loginUserController.handle(request, response);
});
export { routerUserAuth }